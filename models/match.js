var mongoose = require('mongoose'),
    Bet = require('./bet'),
    Schema = mongoose.Schema;

var MatchSchema = new Schema({
    date: Date,
    price: Number,
    scoreTeamA: Number,
    scoreTeamB: Number,
    teamA: {
        type: String,
        required: true
    },
    teamB: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    bets: {
        type: [Bet.schema]
    },
    userId: String
});

var Match = mongoose.model('Match', MatchSchema);

Match.fetchOne = function(req, res) {
    Match.findOne({ _id: req.params.id}, function(err, match) {
        if (err) {
            return res.send(err);
        }

        res.json(match);
    });
};

Match.fetch = function(req, res) {
    Match.find(function(err, matches) {
        if (err) {
            return res.send(err);
        }

        res.json(matches);
    });
}

Match.create = function(req, res) {
    var match = new Match(req.body);

    match.userId = req.user._id;

    match.save(function(err) {
        if (err) {
            return res.send(err);
        }

        res.json(match);
    });
}

Match.update = function(req, res) {
    Match.findOne({ userId: req.user._id, _id: req.params.id }, function(err, match) {
        if (err) {
            return res.send(err);
        }

        if(!match) {
            return res.send("No matches found!");
        }
        for (prop in req.body) {
            match[prop] = req.body[prop];
        }

        // save the match
        match.save(function(err) {
            if (err) {
                return res.send(err);
            }

            res.json(match);
        });
    });
}

Match.delete = function(req, res) {
    Match.findOne({ _id: req.params.id }, function(err, match) {
        if (err) {
            return res.send(err);
        }

        if(!match) {
            return res.sendStatus(204);
        }

        Match.remove({
            _id: req.params.id
        }, function (err, match) {
            if (err) {
                return res.send(err);
            }

            res.json(req.params.id);
        });
    });
}

Match.addBet = function(req, res) {
    Match.findOne({ _id: req.params.idMatch}, function(err, match) {
        if (err) {
            return res.send(err);
        }
        var bet = req.body;
        bet.date = new Date();
        bet.userId = req.user._id;
        match.bets.push(bet);
        match.save(function(err) {
            if (err) {
                return res.send(err);
            }

            res.json(match);
        });
    });
};

module.exports = Match;


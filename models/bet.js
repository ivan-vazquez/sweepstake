var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var BetSchema = new Schema({
    scoreTeamA: {
        type: Number,
        required: true
    },
    scoreTeamB: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    userId: String
});

Bet = mongoose.model('Bet', BetSchema);

module.exports.Bet = Bet;


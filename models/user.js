// Load required packages
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt-nodejs');


// Define our user schema
var UserSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    }
});

// Execute before each user.save() call
UserSchema.pre('save', function(callback) {
    var user = this;

    // Break out if the password hasn't changed
    if (!user.isModified('password')) return callback();

    // Password changed so we need to hash it
    bcrypt.genSalt(5, function(err, salt) {
        if (err) return callback(err);

        bcrypt.hash(user.password, salt, null, function(err, hash) {
            if (err) return callback(err);
            user.password = hash;
            callback();
        });
    });
});

UserSchema.methods.verifyPassword = function(password, cb) {
    bcrypt.compare(password, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

var User = mongoose.model('User', UserSchema);

User.create = function(req, res) {
    var user = new User({
        username: req.body.username,
        password: req.body.password
    });

    user.save(function(err) {
        if (err)
            res.send(err);

        res.json({  // Don't send the password!
            "_id": user._id,
            "username": user.username
        });
    });
};

User.fetchOne = function(req, res) {
    User.findOne({ _id: req.params.id}, function(err, user) {
        if (err) {
            return res.send(err);
        }
        res.json({  // Don't send the password!
            "_id": user._id,
            "username": user.username
        });
    });
};

User.fetch = function(req, res) {
    User.find(function(err, users) {
        if (err)
            res.send(err);

        var safeUsers = [];
        users.forEach((user => {
            var safeUser = {
                "_id": user._id,
                "username": user.username
            }
            safeUsers.push(safeUser);
        }));

        res.json(safeUsers);
    });
}

User.delete = function(req, res) {
    User.findOne({ _id: req.params.id }, function(err, user) {
        if (err) {
            return res.send(err);
        }

        if(!user) {
            return res.sendStatus(204);
        }

        User.remove({
            _id: req.params.id
        }, function (err, user) {
            if (err) {
                return res.send(err);
            }

            res.json(req.params.id);
        });
    });
}


module.exports = User;
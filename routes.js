// Load required packages
var express = require('express');

// List all controllers here
var authController = require('./controllers/auth');
var matchesController = require('./controllers/matches');
var betsController = require('./controllers/bets');
var usersController = require('./controllers/users');

var router = express.Router();

/* Matches */
router.route('/api/matches')
    .get(matchesController.fetch)
    .post(authController.isAuthenticated, matchesController.create);

router.route('/api/matches/:id')
    .get(matchesController.fetchOne)
    .put(authController.isAuthenticated, matchesController.update)
    .delete(authController.isAuthenticated, matchesController.delete);

/* Bets */
router.route('/api/matches/:idMatch/bets')
    .post(authController.isAuthenticated, matchesController.addBet);

router.route('/api/matches/:idMatch/bets/:id')
    .get(betsController.fetchOne)
    .put(authController.isAuthenticated, betsController.update)
    .delete(betsController.delete);

/* Users */
router.route('/api/users')
    .post(usersController.create)
    .get(usersController.fetch);
router.route('/api/users/:id')
    .get(usersController.fetchOne)
    .delete(usersController.delete);


module.exports = router;
// database config
var mongoose = require('mongoose');

var local_uri = 'mongodb://localhost:27017/sweepstakedb';

var connectionString = process.env.MONGOLAB_URI || local_uri;

mongoose.connect(connectionString);
mongoose.connection.on('error', function() {
    console.info('Error: Could not connect to MongoDB. Did you forget to run `mongod`?'.red);
});
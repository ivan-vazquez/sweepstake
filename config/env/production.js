// production error handler

// no stacktraces leaked to user
module.exports = function(app) {

    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: {}
        });
    });
}
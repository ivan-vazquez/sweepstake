
# Sweepstake app

Sports betting SPA

## BackEnd
The technologies used in the backend are: **node.js** with **Express**.
For the data storage it's used **mongodb** and **mongoose** as **ORM**.

The backend provides an API REST. 
It implements two **CRUD controllers** (matches and users). 

The backend uses a **MVC** architecture pattern.

The backend uses a **Repository pattern** to separate the logic that retrieves the data and maps it to the entity model (mongoose ORM) from the business logic that acts on the model.
I think that the business logic should be agnostic from the persistence layer. This pattern allows to write easy tests on the business logic layer.


## FrontEnd

Usage of **React** framework.
// Load required packages
var Bet = require('../models/bet');
var Match = require('../models/match');

exports.fetch = function(req, res) {
    Bet.fetch(req, res);
};

exports.fetchOne = function(req, res) {
    Bet.fetchOne(req, res);
};

exports.delete = function(req, res) {
    Bet.delete(req, res);
};

exports.create = function(req, res) {
    Bet.create(req, res);
};

exports.update = function(req,res){
    // Bets can be edited until 15 minutes before the match
    Match.findOne({ _id: req.params.id}, function(err, match) {
        if (err) {
            return res.send(err);
        }

        var now = new Date();
        var dateLimitChanges = new Date(match.date);

        dateLimitChanges.setMinutes(dateLimitChanges.getMinutes() - 15);

        if (now >= dateLimitChanges) {
            return res.send('Bets are closed!')
        }

        Bet.update(req, res);
    });

};


// Load required packages
var Match = require('../models/match');

exports.fetch = function(req, res) {
    Match.fetch(req, res);
};

exports.fetchOne = function(req, res) {
    Match.fetchOne(req, res);
};

exports.delete = function(req, res) {
    Match.delete(req, res);
};

exports.create = function(req, res) {
    Match.create(req, res);
};

exports.update = function(req,res){
    Match.update(req, res);
};

exports.addBet = function(req, res) {
    Match.addBet(req, res);
}

exports.fetchBets = function(req, res) {
    Match.fetchBets(req, res);
}
// Load required packages
var User = require('../models/user');


exports.create = function(req, res) {
  User.create(req, res);
};

exports.fetch = function(req, res) {
  User.fetch(req, res)
};

exports.fetchOne = function(req, res) {
  User.fetchOne(req, res)
};

exports.delete = function(req, res) {
  User.delete(req, res);
};
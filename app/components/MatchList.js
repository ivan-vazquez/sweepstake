import React from 'react';
import MatchRow from './MatchRow';

class MatchList extends React.Component {

    constructor(props) {
        super(props)
        this.state = { listMatches: [] }
    }

    componentDidMount() {
        fetch('/api/matches')
            .then((response) => {
                return response.json()
            })
            .then((matches) => {
                this.setState({ listMatches: matches })
            })
    }

    render() {

        return (
            <ul className="media-list">
                {
                    this.state.listMatches.map((match) => {
                        var bets = match.bets || [];

                        return <MatchRow key={ match.id }
                                         teamA={ match.teamA }
                                         teamB={ match.teamB }
                                         title={ match.title }
                                         price={ match.price }
                                         bets ={ bets } />
                    })
                }
            </ul>
        );
    }
}

export default MatchList;
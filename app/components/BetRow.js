import React from 'react';

class BetRow extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            bet: this.props.bet,
            user: {
                username: ''
            }
        };
    }

    componentDidMount() {
        fetch('/api/users/' + this.state.bet.userId)
            .then((response) => {
                return response.json()
            })
            .then((user) => {
                this.setState({ user: user })
            })
    }

    render() {
        let date = new Date(this.state.bet.date);
        var strDate = (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear();

        return (
            <li className="media">
                <div className="media-body">
                    <b>{this.state.user.username}: {this.state.bet.scoreTeamA} - {this.state.bet.scoreTeamB}</b>
                    &nbsp;&nbsp;(<i>{strDate}</i>)
                </div>
            </li>
        );
    }
}

export default BetRow;
import React from 'react';
import BetList from './BetList';

class MatchRow extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <li className="media">
                <div className="media-body">
                    <h4>{this.props.title}</h4>
                    <ul>
                        <li>{this.props.teamA} vs {this.props.teamB}</li>
                        <li><span className="label label-info">Price: {this.props.price} €</span></li>
                    </ul>
                    <h4>- Bets:</h4>
                        <BetList bets={this.props.bets} />
                </div>
            </li>
        );
    }
}

export default MatchRow;
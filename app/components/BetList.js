import React from 'react';
import BetRow from './BetRow';

class BetList extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <ul className="media-list">
                {
                    this.props.bets.map((bet) => {
                        return (<BetRow bet={bet} />)
                    })
                }
            </ul>
        );
    }
}

export default BetList;
import React from 'react';
import {IndexRoute} from 'react-router';
import {Route} from 'react-router';
import App from './components/App';
import MatchList from './components/MatchList';
import MatchRow from './components/MatchRow';
import AddUserForm from './components/AddUserForm';
import Index from './components/Index';

export default (
    <Route component={App}>
        <Route path='/' component={Index} />
        <Route path='/matches' component={MatchList} />
        <Route path="/users" component={AddUserForm}/>
    </Route>
);